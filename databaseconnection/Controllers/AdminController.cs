﻿using jindal.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace jindal.Controllers
{
    public class AdminController : Controller
    {
        
        public ActionResult AddDept()
        {
            List<Department> deptlist = new List<Department>();
            using (MySqlConnection asd2 = new MySqlConnection(ConfigurationManager.AppSettings["connectionstring"].ToString()))
            {
                asd2.Open();
                MySqlCommand asd3 = new MySqlCommand();
                asd3.Connection = asd2;
                asd3.CommandText = string.Format("Select * from ReassureRFIDDb.departmentdetails");
                MySqlDataReader dr = asd3.ExecuteReader();
                while(dr.Read())
                {
                    Department d = new Department();
                    d.department_id = Convert.ToInt32(dr[0]);
                    d.department_name = dr[3].ToString();
                    d.department_code = dr[2].ToString();
                    d.company_id = 1;
                    d.active = Convert.ToBoolean(dr[4]);
                    deptlist.Add(d);
                }
               
            }
            return View(deptlist);
        }
        [HttpPost]
        public ActionResult AddDept(Department dept)
        {
            using (MySqlConnection asd2 = new MySqlConnection(ConfigurationManager.AppSettings["connectionstring"].ToString()))
            {
                asd2.Open();

                MySqlCommand asd3 = new MySqlCommand();
                asd3.Connection = asd2;
                asd3.CommandText = string.Format("Insert into  ReassureRFIDDb.departmentdetails (company_id,department_code,dapartment_name,active) values ({0},'{1}','{2}',{3})",1,dept.department_code,dept.department_name,true);
                int i = asd3.ExecuteNonQuery();

            }
                return RedirectToAction("AddDept","Admin");
        }

        public ActionResult AddEmpDesg()
        {
            GenericLists genlist = new GenericLists();
            List<Employee_Desg> empdesglist = new List<Employee_Desg>();
            List<Department> deptlist = new List<Department>();
            using (MySqlConnection asd2 = new MySqlConnection(ConfigurationManager.AppSettings["connectionstring"].ToString()))
            {
                asd2.Open();
                MySqlCommand asd3 = new MySqlCommand();
                asd3.Connection = asd2;
                asd3.CommandText = string.Format("Select * from ReassureRFIDDb.departmentdetails");
                MySqlDataReader dr = asd3.ExecuteReader();
               
                while (dr.Read())
                {
                    Department d = new Department();
                    d.department_id = Convert.ToInt32(dr[0]);
                    d.department_name = dr[3].ToString();
                    d.department_code = dr[2].ToString();
                    d.company_id = 1;
                    d.active = Convert.ToBoolean(dr[4]);
                    deptlist.Add(d);
                }
                asd2.Close();
                asd2.Open();
                genlist.departments = deptlist;
                asd3.Connection = asd2;
                asd3.CommandText= string.Format("select designationdetails.designation_type,designation_id,department_code,`designationdetails`.company_id from designationdetails join departmentdetails where  designationdetails.`department_id`=departmentdetails.`department_id`");
                 dr = asd3.ExecuteReader();
                while (dr.Read())
                {
                    Employee_Desg d = new Employee_Desg();
                    d.designation_id = Convert.ToInt32(dr[1]);
                    d.designation_type = dr[0].ToString();
                    d.department_id = dr[2].ToString();
                    d.company_id = 1;
                    d.active = Convert.ToBoolean(dr[3]);
                    empdesglist.Add(d);
                }

                genlist.designations = empdesglist;

            }
            return View(genlist);
        }
        [HttpPost]
        public ActionResult AddEmpDesg(Employee_Desg empdesg)
        {
            using (MySqlConnection asd2 = new MySqlConnection(ConfigurationManager.AppSettings["connectionstring"].ToString()))
            {
                asd2.Open();

                MySqlCommand asd3 = new MySqlCommand();
                asd3.Connection = asd2;
                asd3.CommandText = string.Format("Insert into  ReassureRFIDDb.designationdetails (company_id,department_id,designation_type,active) values ({0},'{1}','{2}',{3})", 1, empdesg.department_id, empdesg.designation_type, true);
                int i = asd3.ExecuteNonQuery();

            }
            return RedirectToAction("AddDept", "Admin");
        }

    }
}