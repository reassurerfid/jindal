﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web;
using System.Web.Mvc;
using MySql.Data.MySqlClient;
using jindal.Models;

namespace jindal.Controllers
{
    public class REASSUREcontroller : Controller
    {
        List<Class1> cls = new List<Class1>();
        string s = "Data Source=166.62.10.49;Initial Catalog=ReassureRFIDDb; user id=reassure93; password=Reassure@2016;";
        
        
        //Company Details
        public ActionResult inscmp()
        {
            return View();
        }
        
        public ActionResult viewcmp()

        {

            MySqlConnection scon = new MySqlConnection(s);
            MySqlCommand cmd = new MySqlCommand("select * from ReassureRFIDDb.companydetails", scon);
            scon.Open();
            MySqlDataReader def = cmd.ExecuteReader();
            while (def.Read())
            {
                Class1 c1 = new Class1();
                c1.company_name = Convert.ToString(def["company_name"]);
                c1.contact_num = Convert.ToString(def["contact_num"]);
                c1.person_name = Convert.ToString(def["person_name"]);
                c1.location = Convert.ToString(def["location"]);
                c1.login_logo = def["login_logo"].ToString();
                c1.dashboard_logo = def["dashboard_logo"].ToString();
                c1.begin_date = Convert.ToString(def["begin_date"]);
                c1.end_date = Convert.ToString(def["end_date"]);
                c1.active = Convert.ToBoolean(def["active"]);
                cls.Add(c1);
            }
            scon.Close();
            return View(cls);
        }

        public ActionResult sss(string company_name, string contact_num, string person_name, string location, string login_logo, string dashboard_logo, string begin_date, string end_date, int active_)
        {
            MySqlConnection scon = new MySqlConnection(s);
            scon.Open();

            MySqlCommand cmd = new MySqlCommand("insert into ReassureRFIDDb.companydetails (company_name,contact_num,person_name,location,login_logo,dashboard_logo,begin_date,end_date,active) values ('" + company_name + "','" + contact_num + "','" + person_name + "','" + location + "' ,'" + login_logo + "','" + dashboard_logo + "','" + begin_date + "','" + end_date + "'," + active_ + ")", scon);
            int i = cmd.ExecuteNonQuery();
            return RedirectToAction("viewcmp");
        }



        //emp details
        List<empdetailsclass> cls1 = new List<empdetailsclass>();
        public ActionResult insempdetails()
        {
            return View();
        }


        public ActionResult viewempdetails()

        {

            MySqlConnection scon = new MySqlConnection(s);
            MySqlCommand cmd = new MySqlCommand("select * from ReassureRFIDDb.employedetails", scon);
            scon.Open();
            MySqlDataReader def = cmd.ExecuteReader();
            while (def.Read())
            {
                empdetailsclass c2 = new empdetailsclass();
                c2.employe_name = Convert.ToString(def["employe_name"]);
                c2.employe_department = Convert.ToInt32(def["employe_department"]);
                c2.company_id = Convert.ToInt32(def["company_id"]);
                c2.employe_designation = Convert.ToInt32(def["employe_designation"]);
                c2.begin_date = Convert.ToString(def["begin_date"]);
                c2.end_date = Convert.ToString(def["end_date"]);
                c2.employe_tagid = Convert.ToInt32(def["employe_tagid"]);
                c2.employe_type = Convert.ToInt32(def["employe_type"]);
                c2.employe_role = Convert.ToInt32(def["employe_role"]);
                c2.contact_number = Convert.ToString(def["contact_number"]);
                c2.active = Convert.ToBoolean(def["active"]);
                cls1.Add(c2);
            }
            scon.Close();
            return View(cls1);
        }

        public ActionResult ppp(string employe_name, int employe_department, int company_id, int employe_designation, string begin_date, string end_date, int employe_tagid, int employe_type, int employe_role, string contact_number, int active)
        {
            MySqlConnection scon = new MySqlConnection(s);
            scon.Open();

            MySqlCommand cmd = new MySqlCommand("insert into ReassureRFIDDb.employedetails (employe_name,employe_department,company_id,employe_designation, begin_date,end_date, employe_tagid, employe_type,employe_role,contact_number,active) values ('" + employe_name + "'," + employe_department + "," + company_id + "," + employe_designation + " , '" + begin_date + "','" + end_date + "', " + employe_tagid + " ," + employe_type + " ," + employe_role + ", '" + contact_number + "', " + active + ")", scon);
            int i = cmd.ExecuteNonQuery();
            return RedirectToAction("viewempdetails");
        }

        //emptype details
        List<emptypeclass> cls2 = new List<emptypeclass>();
        public ActionResult insemptype()
        {
            return View();
        }


        public ActionResult viewemptype()

        {

            MySqlConnection scon = new MySqlConnection(s);
            MySqlCommand cmd = new MySqlCommand("select * from ReassureRFIDDb.emptype", scon);
            scon.Open();
            MySqlDataReader def = cmd.ExecuteReader();
            while (def.Read())
            {
                emptypeclass c3 = new emptypeclass();
                c3.emp_type = Convert.ToString(def["emp_type"]);
                c3.company_id = Convert.ToInt32(def["company_id"]);
                c3.active = Convert.ToBoolean(def["active"]);
                cls2.Add(c3);
            }
            scon.Close();
            return View(cls2);
        }


        public ActionResult nnn(string emp_type, int company_id, int active)
        {
            MySqlConnection scon = new MySqlConnection(s);
            scon.Open();

            MySqlCommand cmd = new MySqlCommand("insert into ReassureRFIDDb.emptype (emp_type,company_id,active) values ('" + emp_type + "'," + company_id + "," + active + ")", scon);
            int i = cmd.ExecuteNonQuery();
            return RedirectToAction("viewemptype");

        }
        //user type
        List<usertypeclass> cls3 = new List<usertypeclass>();
        

        public ActionResult viewusertype()

        {

            MySqlConnection scon = new MySqlConnection(s);
            MySqlCommand cmd = new MySqlCommand("select * from ReassureRFIDDb.usertype", scon);
            scon.Open();
            MySqlDataReader def = cmd.ExecuteReader();
            while (def.Read())
            {
                usertypeclass c4 = new usertypeclass();
                c4.assaign_type = Convert.ToString(def["assaign_type"]);
                c4.company_id = Convert.ToInt32(def["company_id"]);
                c4.active = Convert.ToBoolean(def["active"]);
                cls3.Add(c4);
            }
            scon.Close();
            return View(cls3);
        }

        public ActionResult aaa(string assaign_type, int company_id, int active)
        {
            MySqlConnection scon = new MySqlConnection(s);
            scon.Open();

            MySqlCommand cmd = new MySqlCommand("insert into ReassureRFIDDb.usertype (assaign_type,company_id,active) values ('" + assaign_type + "'," + company_id + "," + active + ")", scon);
            int i = cmd.ExecuteNonQuery();
            return RedirectToAction("viewemptype");

        }

        List<rfiddetails> cls4 = new List<rfiddetails>();
        public ActionResult insrfid()
        {
            return View();
        }


        public ActionResult viewrfid()

        {

            MySqlConnection scon = new MySqlConnection(s);
            MySqlCommand cmd = new MySqlCommand("select * from ReassureRFIDDb.RFIDdetails", scon);
            scon.Open();
            MySqlDataReader def = cmd.ExecuteReader();
            while (def.Read())
            {
                rfiddetails c5 = new rfiddetails();
                c5.company_id = Convert.ToInt32(def["company_id"]);
                c5.rfid_tagid = Convert.ToString(def["rfid_tagid"]);
                c5.rfid_issue_date = Convert.ToString(def["rfid_issue_date"]);
                c5.rfid_start_date = Convert.ToString(def["rfid_start_date"]);
                c5.rfid_assaigned_type = Convert.ToInt32(def["rfid_assaigned_type"]);
                c5.rfid_assaigned_id = Convert.ToInt32(def["rfid_assaigned_id"]);
                c5.rfid_issued_by = Convert.ToString(def["rfid_issued_by"]);
                c5.active = Convert.ToBoolean(def["active"]);
                cls4.Add(c5);
            }
            scon.Close();
            return View(cls4);
        }
        public ActionResult ccc( int company_id, string rfid_tagid, string rfid_issue_date, string rfid_start_date,int rfid_assaigned_type, int rfid_assaigned_id, string rfid_issued_by, Boolean active)
        {
            MySqlConnection scon = new MySqlConnection(s);
            scon.Open();

            MySqlCommand cmd = new MySqlCommand("insert into ReassureRFIDDb.RFIDdetails (company_id,rfid_tagid,rfid_issue_date,rfid_start_date,rfid_assaigned_type,rfid_assaigned_id,rfid_issued_by, active) values (" +company_id + ",'" + rfid_tagid + "', '" + rfid_issue_date + "','" + rfid_start_date + "', " + rfid_assaigned_type + "," + rfid_assaigned_id + ", '" + rfid_issued_by + "',  " + active + ")", scon);
            int i = cmd.ExecuteNonQuery();
            return RedirectToAction("viewrfid");

        }
        List<designationclass> cls5 = new List<designationclass>();


        public ActionResult viewdesignation()

        {

            MySqlConnection scon = new MySqlConnection(s);
            MySqlCommand cmd = new MySqlCommand("select * from ReassureRFIDDb.designationdetails", scon);
            scon.Open();
            MySqlDataReader def = cmd.ExecuteReader();
            while (def.Read())
            {
                designationclass c6 = new designationclass();
                c6.company_id = Convert.ToInt32(def["company_id"]);
                c6.department_id = Convert.ToInt32(def["department_id"]);
                c6.designation_type = Convert.ToString(def["designation_type"]);
                c6.active = Convert.ToBoolean(def["active"]);
                cls5.Add(c6);
            }
            scon.Close();
            return View(cls3);
        }
        public ActionResult mmm(int company_id, int department_id,int designation_type,int active)
        {
            MySqlConnection scon = new MySqlConnection(s);
            scon.Open();

            MySqlCommand cmd = new MySqlCommand("insert into ReassureRFIDDb.designationdetails (company_id,department_id,designation_type,active) values (" + company_id + ", " + department_id +", '" + designation_type +"'," + active + ")", scon);
            int i = cmd.ExecuteNonQuery();
            return RedirectToAction("viewdesignation");

        }
        //public ActionResult Login()
        //{
        //    return View();
        //}
        //[HttpPost]
        //public ActionResult Login(string username, string password)
        //{
        //    if (username == "rfid@gmail.com" && password == "123456")
        //    {
        //        return RedirectToAction("viewemptype", "REASSURE");
        //    }
        //    else
        //    {
        //        return View("Login");
        //    }
        //}
        List<enrollclass> cls6 = new List<enrollclass>();


        public ActionResult viewenrollement()

        {

            MySqlConnection scon = new MySqlConnection(s);
            MySqlCommand cmd = new MySqlCommand("select * from ReassureRFIDDb.gate_enrollement", scon);
            scon.Open();
            MySqlDataReader def = cmd.ExecuteReader();
            while (def.Read())
            {
                enrollclass c7 = new enrollclass();
                c7.gate_name = Convert.ToString(def["gate_name"]);
                c7.gate_purpose = Convert.ToString(def["gate_purpose"]);
                c7.gate_reader_id = Convert.ToString(def["gate_reader_id"]);
                           }
            scon.Close();
            return View(cls6);
        }
        public ActionResult bbb(int company_id, int department_id, int designation_type, int active)
        {
            MySqlConnection scon = new MySqlConnection(s);
            scon.Open();

            MySqlCommand cmd = new MySqlCommand("insert into ReassureRFIDDb.designationdetails (company_id,department_id,designation_type,active) values (" + company_id + ", " + department_id + ", '" + designation_type + "'," + active + ")", scon);
            int i = cmd.ExecuteNonQuery();
            return RedirectToAction("viewenrollement");

        }

    }
}