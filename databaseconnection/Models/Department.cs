﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace jindal.Models
{
    public class Department
    {
        [Key]
        public int department_id { set; get; }
        public int company_id { set; get; }
        public string department_code { set; get; }
        public string department_name { set; get; }
        public Boolean active { set; get; }

    }
}