﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace jindal.Models
{
    public class Class1
    {
        public string company_name { set; get; }
        public string contact_num { set; get; }
        public string person_name { set; get; }
        public string location { set; get; }
        public string login_logo { set; get; }
        public string dashboard_logo { set; get; }
        public string begin_date { set; get; }
        public string end_date { set; get; }
        public Boolean active { set; get; }

    }
    public class empdetailsclass
    {

        public string employe_name { set; get; }
        public int employe_department { set; get; }
        public int company_id { set; get; }
        public int employe_designation { set; get; }
        public string begin_date { set; get; }
        public string end_date { set; get; }
        public int employe_tagid { set; get; }
        public int employe_type { set; get; }
        public int employe_role { set; get; }
        public string contact_number { set; get; }
        public Boolean active { set; get; }


    }
    public class emptypeclass
    {
        public string emp_type { set; get; }
        public int company_id { set; get; }
        public Boolean active { set; get; }
    }
    public class usertypeclass
    {
        public string assaign_type { set; get; }
        public int company_id { set; get; }
        public Boolean active { set; get; }
    }

    public class rfiddetails
    {

        public int company_id { set; get; }
        public string rfid_tagid { set; get; }
        public string rfid_issue_date { set; get; }
        public string rfid_start_date { set; get; }
        public int rfid_assaigned_type { set; get; }
        public int rfid_assaigned_id { set; get; }
        public string rfid_issued_by { set; get; }

        public Boolean active { set; get; }
    }
    public class designationclass
    {
        public int company_id { set; get; }
        public int department_id { set; get; }
        public string designation_type { set; get; }
        public Boolean active { set; get; }
    }
    public class enrollclass
    {

        public string gate_name { set; get; }
        public string gate_purpose { set; get; }
        public string gate_reader_id { set; get; }
    }
}