﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace jindal.Models
{
    public class Employee_Desg
    {
        [Key]
        public int designation_id { set; get; }
        public int company_id { set; get; }
        public string department_id { set; get; }
        public string designation_type { set; get; }
        public Boolean active { set; get; }
    }
}